<!DOCTYPE html>
<html lang ="es">

<head>
    <meta charset="UTF-8">
    <title>Ejecutando PHP</title>
</head>

<body>
    <h1>probando las primeras sentencias en PHP</h1>
    <h3> 
        <?php
        echo 'HOLA MUNDO';
        ?>
    </h3>
    <?php
    $mundo ='MUNDO';

    // otra forma de mostrar lo mismo
    echo "<h3> HOLA $mundo</h3>";

    //y otra forma de mostrar lo mismo
    echo '<h3> HOLA'.$mundo.'</h3>';
    ?>
</body>

</html>